package org.udg.caes.stockmarket;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.udg.caes.stockmarket.NOTEST.Fake_PS_MySQL;
import org.udg.caes.stockmarket.NOTEST.OrderServiceImpl;
import org.udg.caes.stockmarket.NOTEST.FakeEUR_SS_Impl;
import org.udg.caes.stockmarket.NOTEST.FakeUSA_SS_Impl;

import java.util.Date;
import java.util.List;


class USAModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(OrderService.class).to(FakeUSA_SS_Impl.class);
    bind(PersistenceService.class).to(Fake_PS_MySQL.class);
  }
}

class EURModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(OrderService.class).to(FakeEUR_SS_Impl.class);
    bind(PersistenceService.class).to(Fake_PS_MySQL.class);
  }
}



public class Main {
  static public void main(String args[]) throws Exception {
    Injector injector = Guice.createInjector(new USAModule());
    BusinessService bs = injector.getInstance(BusinessService.class);
    UtilsService us = injector.getInstance(UtilsService.class);

    User u1 = new User("Jordi");
    User u2 = new User("Pep");

    bs.saveUser(u1);
    bs.saveUser(u2);

    Portfolio p11 = new Portfolio(u1, "USA1");
    Portfolio p12 = new Portfolio(u1, "USA2");

    bs.savePortfolio(p11);
    bs.savePortfolio(p12);

    Date now = new Date();

    Order o11 = new Order(p11, 100, new Date(now.getTime() + 86400000L), "NVDA");
    bs.placeOrder(o11);

    // BEGIN IGNORE
    // This code should never exists. It simulates the asynchronous response from the broker
    OrderServiceImpl ss = (OrderServiceImpl)us.getStockService();
    ss.simulateOrderProcessing(o11.getId());
    if (o11.getStatus() == Order.ACCEPTED)
      ss.simulateOrderExecution(o11.getId());
    // End of asynchronous response
    // END IGNORE

    Portfolio pp1 = bs.getPortfolio(u1.getId(), p11.getId());
    double v11 = bs.getPortfolioValuation(u1.getId(), pp1.getId());

    p12.addStock(new Stock("AMD", 500, 3.2));
    bs.savePortfolio(p12);

    double v12 = bs.getPortfolioValuation(u1.getId(), p12.getId());

    Portfolio pbest = bs.getBestPortfolio(u1.getId());

    Portfolio p21 = new Portfolio(u2, "USA1");
    Portfolio p22 = new Portfolio(u2, "USA2");

    bs.savePortfolio(p21);
    bs.savePortfolio(p22);

    p21.addStock(new Stock("NVDA", 1200, 14.9));
    bs.savePortfolio(p21);

    Portfolio pp2 = bs.getPortfolio(u2.getId(), p21.getId());
    double v21 = bs.getPortfolioValuation(u2.getId(), pp2.getId());

    p22.addStock(new Stock("FB", 2100, 54.4));
    bs.savePortfolio(p22);

    List<String> common = bs.getCommonStocks(u1.getId(), u2.getId());

    List<String> ss1 = us.searchStocks("N");
    List<String> ss2 = us.searchStocksWithValue(150.0);

    System.out.println("Bye!");
  }
}
