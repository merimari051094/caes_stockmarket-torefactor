package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import org.udg.caes.stockmarket.NOTEST.Fake_PS_MySQL;
import org.udg.caes.stockmarket.NOTEST.OrderServiceImpl;
import org.udg.caes.stockmarket.exceptions.*;

import java.util.ArrayList;
import java.util.List;

/**
 * This class contains the business logic of the app.
 * It resembles a EJB from Java EE 7
 */
public class BusinessService {
  PersistenceService mPS;
  OrderServiceImpl mSS;
  private Integer orderId = 0;

  @Inject
  public BusinessService(PersistenceService ps, OrderServiceImpl ss) {
    mPS = ps;
    mSS = ss;
  }

  public User getUser(String id) throws EntityNotFound {
    return mPS.getUser(id);
  }

  public Portfolio getPortfolio(String userId, String name) throws EntityNotFound, ElementNotExists {
    User u = mPS.getUser(userId);
    return u.getPortfolio(name);
  }

  public void saveUser(User u) {
    mPS.saveUser(u);
  }

  public void savePortfolio(Portfolio p) throws ElementNotExists, EntityNotFound, ElementAlreadyExists, InvalidOperation {
    mPS.savePortfolio(p);
  }

  public Boolean hasStock(String userId, String ticker) throws EntityNotFound {
    User u = mPS.getUser(userId);
    for (Portfolio p: u.getAllPortfolios()) {
      if (p.hasStock(ticker))
        return true;
    }
    return false;
  }

  public Double getUserValuation(String userId) throws EntityNotFound, StockNotFound{
    User u = mPS.getUser(userId);
    double v = 0.0;
    for (Portfolio p: u.getAllPortfolios()) {
      for (Stock s : p)
        v += s.getQuantity() * mSS.getPrice(s.getName());
    }
    return v;
  }

  public Double getPortfolioValuation(String userId, String pname) throws EntityNotFound, ElementNotExists, StockNotFound {
    User u = mPS.getUser(userId);
    Portfolio p = u.getPortfolio(pname);
    double v = 0.0;
    for (Stock s : p)
      v += s.getQuantity() * mSS.getPrice(s.getName());
    return v;
  }

  public Portfolio getBestPortfolio(String userId) throws StockNotFound, EntityNotFound {
    User u = mPS.getUser(userId);
    double max = -Double.MAX_VALUE;
    Portfolio result = null;
    for (Portfolio p: u.getAllPortfolios()) {
      double profit = 0.0;
      for (Stock s : p)
        profit += s.getQuantity() * (mSS.getPrice(s.getName()) - s.getBuyPrice());
      if (profit > max) {
        max = profit;
        result = p;
      }
    }
    return result;
  }

  public List<String> getCommonStocks(String userId1, String userId2) throws EntityNotFound {
    User u1 = mPS.getUser(userId1);
    User u2 = mPS.getUser(userId2);
    ArrayList<String> stocks = new ArrayList<String>();
    for (Portfolio p: u1.getAllPortfolios()) {
      for (Stock s : p)
        if (!stocks.contains(s.getName()) && u2.hasStock(s.getName()))
          stocks.add(s.getName());
    }
    return stocks;
  }

  /**
   * This method sends an Order for a give User
   *
   * @param o Order
   * @throws OrderNotSent
   */
  public void placeOrder(Order o) throws NoSuchMethodException {
    orderId++;
    o.setId(orderId.toString());
    o.setStatus(Order.PROCESSING);
    mPS.saveOrder(o);
    o.send(mSS);
  }

  public OrderService getStockService() {
    return mSS;
  }

  /**
   * This method is called every time an Order has been processed. The order comes with the field status modified appropriately.
   * @param order Order processed
   */
  public static void processingCallback(Order order) {
    Fake_PS_MySQL ps = new Fake_PS_MySQL();
    ps.saveOrder(order);
  }

  /**
   * This method is called every time an Order has been executed. The order comes with the field status modified appropriately.
   * @param order Order processed
   */
  public static void executionCallback(Order order) {
    Fake_PS_MySQL ps = new Fake_PS_MySQL();
    if (order.getStatus() == Order.COMPLETED || order.getStatus() == Order.PARTIALLY_COMPLETED) {
      int efq = order.getEfQuant();
      double efp = order.getEfPrice();
      String ticker = order.getTicker();
      Stock s = new Stock(ticker, efq, efp);
      order.getPortfolio().addStock(s);
    }
    ps.saveOrder(order);
  }
}
