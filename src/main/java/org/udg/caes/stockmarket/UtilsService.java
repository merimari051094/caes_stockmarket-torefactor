package org.udg.caes.stockmarket;

import com.google.inject.Inject;
import org.udg.caes.stockmarket.exceptions.StockNotFound;
import org.udg.caes.stockmarket.NOTEST.Fake_PS_MySQL;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imartin on 9/12/13.
 */
public class UtilsService {


  OrderService mSS;

  @Inject
  public UtilsService(OrderService ss) {
    mSS = ss;
  }

  public OrderService getStockService() {
    return mSS;
  }


  public ArrayList<String> searchStocks(String substring) {
    ArrayList<String> r = new ArrayList<String>();

    for (StockMarket sm: mSS.getMarkets())
        r.addAll(sm.getAllStocks(substring));

    return r;
  }

  public List<String> searchStocksWithValue(double price) {
    ArrayList<String> r = new ArrayList<String>();

    for (StockMarket sm: mSS.getMarkets())
      for (String s : sm.getAllStocks())
        try {
          if (mSS.getPrice(s) > price)
            r.add(s);
        } catch (StockNotFound stockNotFound) {
        }

    return r;
  }




}
