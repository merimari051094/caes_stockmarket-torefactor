package org.udg.caes.stockmarket.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.udg.caes.stockmarket.Stock;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by David on 14/01/2016.
 * Test for Stock
 */
@RunWith( Parameterized.class )
public class TestStock {
    protected Stock s;
    @Before
    public void inisialitzar(){
        s=new Stock("Prova",36, 42.42);
    }
    @Test
    public void getBuyPriceTest(){
        assertEquals(s.getBuyPrice(), 42.42, 0);
    }

    @Test
    public void getNameTest(){
        assertEquals(s.getName(), "Prova");
    }

    @Test
    public void setNameTest(){
        s.setName("Prova2");
        assertEquals(s.getName(), "Prova2");
    }

    @Test
    public void getQuantityTest(){
        assertEquals(s.getQuantity(), 36);
    }

    @Test
    public void getPriceTest(){
        assertEquals(s.getPrice(), 42.42d, 0);
    }

    private final Double a;
    private final Double b;
    @Parameterized.Parameters
    public static List<Double[]> data() {
        return Arrays.asList( new Double[][] {
                { 42d, 42d},
                { 40d, 40d},
                { 234.21d, 234.21d}
        });
    }
    public TestStock(Double a, Double b ){
        this.a = a;
        this.b = b;
    }
    @Test
    public void setPriceTest(){
        s.setBuyPrice(a);
        assertEquals(s.getPrice(), b, 0);
    }
    @Test
    public void setBuyPriceTest(){
        s.setBuyPrice(a);
        assertEquals(s.getBuyPrice(), b, 0);
    }
    @Test
    public void setQuantityTest(){
        s.setQuantity(a.intValue());
        assertEquals(s.getQuantity(), b.intValue());
    }
}
