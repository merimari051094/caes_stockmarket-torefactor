package org.udg.caes.stockmarket.test;

import mockit.Expectations;
import mockit.Injectable;
import org.junit.Before;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.NOTEST.Fake_SM_Impl;
import org.udg.caes.stockmarket.NOTEST.OrderServiceImpl;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by David on 9/12/13.
 * Tests for Utils services
 */
public class TestUtilsService {
    @Injectable protected OrderServiceImpl os=new OrderServiceImpl("Test");
    final protected UtilsService us=new UtilsService(os);
    @Injectable StockMarket smi;
    // OrderServiceImpl osi;
    Fake_SM_Impl sm;
    @Before
    public void inisialitzar(){
        sm=new Fake_SM_Impl("Market1");
        sm.addStocks("patates");
        sm.addStocks("pate");
        sm.addStocks("prit");
        os.addMarket(sm);
    }
    @Test
    public void StockService(){
        assertEquals(us.getStockService(), os);
    }

    @Test
    public void searchProductName(){
        new Expectations(){{
            os.getMarkets(); result=sm;
        }};
        ArrayList<String> items=us.searchStocks("pat");
        ArrayList<String> compare=new ArrayList<String>();
        compare.add("patates");
        compare.add("pate");
        assertEquals(items,compare);
    }
    @Test
    public void searchNoProductName(){
        new Expectations(){{
            os.getMarkets(); result=sm;
        }};
        ArrayList<String> items=us.searchStocks("usb");
        ArrayList<String> compare=new ArrayList<String>();
        assertEquals(items,compare);
    }
    @Test
    public void searchProductPrice() throws StockNotFound {
        new Expectations(){{
            os.getMarkets(); result=sm;
            os.getPrice("patates"); result=26d;
            os.getPrice("pate"); result=26d;
            os.getPrice("prit"); result=23d;
        }};
        List<String> items=us.searchStocksWithValue(25d);
        ArrayList<String> compare=new ArrayList<String>();
        compare.add("patates");
        compare.add("pate");
        assertEquals(items,compare);
    }
    @Test
    public void searchNoProductPrice() throws StockNotFound {
        new Expectations(){{
            os.getMarkets(); result=sm;
            os.getPrice("patates"); result=26d;
            os.getPrice("pate"); result=26d;
            os.getPrice("prit"); result=23d;
        }};
        List<String> items=us.searchStocksWithValue(30d);
        ArrayList<String> compare=new ArrayList<String>();
        assertEquals(items,compare);
    }
}