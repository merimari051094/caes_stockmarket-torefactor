package org.udg.caes.stockmarket.test;

import mockit.Expectations;
import mockit.Verifications;
import org.junit.Test;
import org.udg.caes.stockmarket.Portfolio;
import org.udg.caes.stockmarket.User;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import static org.junit.Assert.*;
/**
 * Created by Meriem on 14/01/2016.
 */
public class TestAddPortfolio {
    private User s;
    private Portfolio p;

    @Test
    public void TestAddPortfolioOK() throws ElementAlreadyExists {
        new Expectations(){{
            s = new User("p");
            p = new Portfolio(s, "paper");
            s.addPortfolio(p);
        }};

        new Verifications() {{
            s.hasPortfolio("paper");
        }};
    }

    @Test
    public void TestPortfolioExist() throws ElementAlreadyExists {
        new Expectations(){{
            s = new User("p");
            p = new Portfolio(s, "p");
            s.addPortfolio(p);
        }};

        new Verifications() {{
            assertTrue(s.hasPortfolio(p.getId()));
        }};
    }
}
