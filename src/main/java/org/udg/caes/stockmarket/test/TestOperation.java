package org.udg.caes.stockmarket.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.udg.caes.stockmarket.Operation;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Patofet on 29/01/2016.
 */
@RunWith( Parameterized.class )
public class TestOperation {
    Operation op;
    @Before
    public void inisialitzar(){
        op=new Operation(3,42.2d);
    }
    @Test
    public void getQuantityTest(){
        assertEquals(op.getQuantity(),3);
    }
    @Test
    public void getBuyPriceTest(){
        assertEquals(op.getBuyPrice(),42.2d,0);
    }
    @Test
    public void getmStatusTest(){
        assertEquals(op.getmStatus(),Operation.Status.PENDING);
    }
    @Test
    public void setmStatusTest(){
        Operation op1=new Operation(3,42.2d,Operation.Status.PARTIAL);
        assertEquals(op1.getmStatus(),Operation.Status.PARTIAL);
    }
    private final Double a;
    private final Double b;
    @Parameterized.Parameters
    public static List<Double[]> data() {
        return Arrays.asList( new Double[][] {
                { 42d, 42d},
                { 40d, 40d},
                { 234.21d, 234.21d}
        });
    }
    public TestOperation(Double a, Double b ){
        this.a = a;
        this.b = b;
    }
    @Test
    public void setQuantityTest(){
        op.setQuantity(a.intValue());
        assertEquals(op.getQuantity(),b.intValue());
    }
    @Test
    public void setBuyPriceTest(){
        op.setBuyPrice(a);
        assertEquals(op.getBuyPrice(),b,0);
    }
}
