package org.udg.caes.stockmarket.test;

import mockit.Expectations;
import mockit.Verifications;
import org.junit.Test;
import org.udg.caes.stockmarket.Portfolio;
import org.udg.caes.stockmarket.User;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;

import static org.junit.Assert.assertEquals;

/**
 * Created by Meriem on 31/01/2016.
 */
public class TestGetPortfolio {

    private User s;
    private Portfolio p;

    @Test
    public void TestGetPortfolioOk() throws ElementAlreadyExists, ElementNotExists {
        new Expectations(){{
            s = new User("p");
            p = new Portfolio(s, "paper");
            s.addPortfolio(p);
        }};

        new Verifications() {{
            assertEquals(s.getPortfolio("paper"), p);
        }};
    }

    @Test
    public void TestGetPortfolioNotExist() throws ElementAlreadyExists, ElementNotExists {
        new Expectations(){{
            s = new User("p");
            p = new Portfolio(s, "paper");
            s.addPortfolio(p);
        }};

        new Verifications() {{
            assertEquals(s.getPortfolio("paper"), p);
        }};
    }
}
