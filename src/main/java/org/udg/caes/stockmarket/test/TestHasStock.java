package org.udg.caes.stockmarket.test;

import mockit.Expectations;
import mockit.Verifications;
import org.junit.Test;
import org.udg.caes.stockmarket.Portfolio;
import org.udg.caes.stockmarket.User;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;

import static org.junit.Assert.assertEquals;

/**
 * Created by Meriem on 31/01/2016.
 */
public class TestHasStock {
    private User s;
    private Portfolio p;

    @Test
    public void TestHasStockOKYes() throws ElementAlreadyExists {
        new Expectations(){{
            s = new User("p");
            p = new Portfolio(s, "paper");
            s.addPortfolio(p);
        }};

        new Verifications() {{
            assertEquals(s.hasPortfolio("paper"), true);
        }};
    }

    @Test
    public void TestHasStockOKNo() throws ElementAlreadyExists {
        new Expectations(){{
            s = new User("p");
            p = new Portfolio(s, "paper");
            s.addPortfolio(p);
        }};

        new Verifications() {{
            assertEquals(s.hasPortfolio("p"), false);
        }};
    }
}
