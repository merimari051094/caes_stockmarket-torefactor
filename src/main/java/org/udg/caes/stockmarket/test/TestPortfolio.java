package org.udg.caes.stockmarket.test;

import mockit.Expectations;
import mockit.Verifications;
import org.junit.Test;
import org.udg.caes.stockmarket.Portfolio;
import org.udg.caes.stockmarket.Stock;
import org.udg.caes.stockmarket.User;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Meriem on 31/01/2016.
 */
public class TestPortfolio {
    private Portfolio p;
    private User s;
    private User u2;
    private List<Stock> sto;
    private Stock st;

    @Test
    public void TestPortfolioOk() {
        p = new Portfolio(s,"paper");
        assertEquals(p.getId(),"paper");
        assertEquals(p.getUser(),s);
    }

    @Test
    public void TestgetUserOk() {
        p = new Portfolio(s,"paper");
        assertEquals(p.getUser(),s);
    }

    @Test
    public void TestSetUserOk() {
        new Expectations(){{
            p = new Portfolio(s, "paper");
            u2 = new User("u2");
            p.setUser(u2);
        }};
        new Verifications(){{
            assertEquals(p.getUser(), u2);
        }};
    }

    @Test
    public void TestgetIdOk() {
        p = new Portfolio(s,"paper");
        assertEquals(p.getId(),"paper");
    }

    @Test
    public void TestSetIdOk() {
        new Expectations(){{
            p = new Portfolio(s, "paper");
            p.setId("p");
        }};
        new Verifications(){{
            assertEquals(p.getId(), "p");
        }};
    }

    @Test
    public void TestAddStocks() {
        new Expectations() {{
            st = new Stock("p", 30, 20.2);
            sto = new ArrayList<Stock>();
            sto.add(st);
            p = new Portfolio(s, "paper");
            p.addStock(st);
        }};
        new Verifications(){{
            assertEquals(p.getStocks(), sto);
        }};
    }

    @Test
    public void TestSetStocks() {
        new Expectations() {{
            Stock st1 = new Stock("p1", 40, 20.2);
            sto = new ArrayList<Stock>();
            sto.add(st1);
            st = new Stock("p", 30, 20.2);
            p = new Portfolio(s, "paper");
            p.addStock(st);
            p.setStocks(sto);
        }};
        new Verifications(){{
            assertEquals(p.getStocks(), sto);
        }};
    }

    @Test
    public void TestHasStockOK() {
        new Expectations(){{
            st = new Stock("p",30,20.2);
            p = new Portfolio(s, "paper");
            p.addStock(st);
        }};
        new Verifications(){{
            assertEquals(p.hasStock("p"), true);
        }};
    }

    @Test
    public void TestHasNotStock() {
        new Expectations(){{
            st = new Stock("p",30,20.2);
            p = new Portfolio(s, "paper");
            p.addStock(st);
        }};
        new Verifications(){{
            assertEquals(p.hasStock("p1"), false);
        }};
    }
}
