package org.udg.caes.stockmarket.test;

import mockit.Expectations;
import mockit.Injectable;
import org.junit.Before;
import org.junit.Test;
import org.udg.caes.stockmarket.*;
import org.udg.caes.stockmarket.NOTEST.Fake_PS_MySQL;
import org.udg.caes.stockmarket.NOTEST.Fake_SM_Impl;
import org.udg.caes.stockmarket.NOTEST.OrderServiceImpl;
import org.udg.caes.stockmarket.exceptions.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Patofet on 30/01/2016.
 */
public class TestBusinessService {
    protected PersistenceService ps;
    @Injectable final OrderServiceImpl os=new OrderServiceImpl("Test");
    BusinessService bs;
    Portfolio p1;
    Portfolio p2;
    User u1;
    @Before
    public void inisialitzar() throws ElementNotExists, InvalidOperation, ElementAlreadyExists, EntityNotFound {
        u1 = new User("Test1");
        User u2 = new User("Test2");
        p1 = new Portfolio(u1,"Prot1");
        p2 = new Portfolio(u2,"Prot2");
        ps=new Fake_PS_MySQL();
        ps.saveUser(u1);
        ps.saveUser(u2);
        p1.addStock(new Stock("Stock1",3,42d));
        p1.addStock(new Stock("Stock2",5,42d));
        p1.addStock(new Stock("Stock3",73,42d));
        p2.addStock(new Stock("Stock1",3,42d));
        p2.addStock(new Stock("Stock3",73,42d));
        ps.savePortfolio(p1);
        ps.savePortfolio(p2);
        Fake_SM_Impl sm1 = new Fake_SM_Impl("Stock1");
        sm1.addStocks("Stock1");
        Fake_SM_Impl sm2 = new Fake_SM_Impl("Stock2");
        sm2.addStocks("Stock2");
        Fake_SM_Impl sm3 = new Fake_SM_Impl("Stock3");
        sm2.addStocks("Stock3");
        os.addMarket(sm1);
        os.addMarket(sm2);
        os.addMarket(sm3);
        bs=new BusinessService(ps,os);
    }
    @Test
    public void getUserTest() throws EntityNotFound {
        assertEquals(bs.getUser("Test1"),u1);
    }
    @Test
    public void saveUserTest() throws EntityNotFound {
        User u = new User("Test");
        bs.saveUser(u);
        assertEquals(bs.getUser("Test"),u);
    }
    @Test
    public void getPortfolioTest() throws ElementNotExists, EntityNotFound {
        assertEquals(bs.getPortfolio("Test1","Prot1"),p1);
    }
    @Test
    public void savePortfolioTest() throws EntityNotFound, ElementAlreadyExists, ElementNotExists, InvalidOperation {
        Portfolio p = new Portfolio(u1,"Portafolio");
        bs.savePortfolio(p);
        assertEquals(bs.getPortfolio("Test1","Portafolio"),p);
    }
    @Test
    public void hasStockTest() throws Exception {
        assertTrue(bs.hasStock("Test1","Stock1"));
    }

    @Test
    public void getUserValuationTest() throws Exception {
        new Expectations(){{
            os.getPrice(anyString); result = 30d;
        }};
        assertEquals(bs.getUserValuation("Test1"),2430d,0);
    }
    @Test(expected = StockNotFound.class)
    public void getUserValuationNotStockTest() throws Exception {
        new Expectations(){{
            os.getPrice(anyString); result = new StockNotFound();
        }};
        assertEquals(bs.getUserValuation("Test1"),2430d,0);
    }
    @Test(expected = EntityNotFound.class)
    public void getUserValuationNotEntityTest() throws Exception {
        assertEquals(bs.getUserValuation("Test"),2430d,0);
    }

    @Test
    public void getPortfolioValuationTest() throws Exception {
        new Expectations(){{
            os.getPrice(anyString); result = 30d;
        }};
        assertEquals(bs.getPortfolioValuation("Test2","Prot2"),2280d,0);
    }
    @Test(expected = StockNotFound.class)
    public void getPortfolioValuationStockNotFoundTest() throws Exception {
        new Expectations(){{
            os.getPrice(anyString); result = new StockNotFound();
        }};
        assertEquals(bs.getPortfolioValuation("Test2","Prot2"),2280d,0);
    }
    @Test(expected = ElementNotExists.class)
    public void getPortfolioValuationNotElementTest() throws Exception {
        assertEquals(bs.getPortfolioValuation("Test2","Prot"),2280d,0);
    }
    @Test(expected = EntityNotFound.class)
    public void getPortfolioValuationNotEntityTest() throws Exception {
        assertEquals(bs.getPortfolioValuation("Test","Prot2"),2280d,0);
    }

    @Test
    public void getBestPortfolioTest() throws StockNotFound, EntityNotFound {
        assertEquals(bs.getBestPortfolio("Test1"),p1);
    }
    @Test(expected = EntityNotFound.class)
    public void getBestPortfolioNotFoundTest() throws StockNotFound, EntityNotFound {
        assertEquals(bs.getBestPortfolio("Test"),p1);
    }
    @Test(expected = StockNotFound.class)
    public void getBestPortfolioStockNotFoundTest() throws StockNotFound, EntityNotFound {
        new Expectations(){{
            os.getPrice(anyString); result = new StockNotFound();
        }};
        assertEquals(bs.getBestPortfolio("Test1"),p1);
    }
    @Test(expected = EntityNotFound.class)
    public void getBestPortfolioNoEntityTest() throws StockNotFound, EntityNotFound {
        assertEquals(bs.getBestPortfolio("Test"),p1);
    }
    @Test(expected = StockNotFound.class)
    public void getBestPortfolioNoStockTest() throws StockNotFound, EntityNotFound {
        new Expectations(){{
            os.getPrice(anyString); result = new StockNotFound();
        }};
        assertEquals(bs.getBestPortfolio("Test1"),p1);
    }
    @Test
    public void getCommonStocksTest() throws EntityNotFound {
        List<String> ls1 = bs.getCommonStocks("Test1","Test2");
        List<String> ls2 = new ArrayList<String>();
        ls2.add("Stock1");
        ls2.add("Stock3");
        assertEquals(ls2,ls1);
    }
    @Test(expected = EntityNotFound.class)
    public void getCommonStockNoUser1Test() throws EntityNotFound {
        List<String> ls1 = bs.getCommonStocks("Test1","Test");
    }
    @Test(expected = EntityNotFound.class)
    public void getCommonStockNoUser2Test() throws EntityNotFound {
        List<String> ls1 = bs.getCommonStocks("Test","Test2");
    }
    @Test(expected = EntityNotFound.class)
    public void getCommonStockNoUsersTest() throws EntityNotFound {
        List<String> ls1 = bs.getCommonStocks("Test","Tests");
    }
    @Test
    public void placeOrderTest() throws NoSuchMethodException {
        Portfolio pf=new Portfolio(new User("Test id"),"test");
        Order o=new Order(pf, 3, new Date(),"prova");
        bs.placeOrder(o);
        assertEquals(o.getStatus(), Order.PROCESSING);
    }
    @Test
    public void getStockServiceTest(){
        assertEquals(bs.getStockService(),os);
    }

    @Test
    public void processingCallbackTest() {
        Portfolio pf=new Portfolio(new User("Test id"),"test");
        Order o=new Order(pf, 3, new Date(),"prova");
        bs.processingCallback(o);
        assertEquals(o.getStatus(), Order.PROCESSING);
    }
}
