package org.udg.caes.stockmarket.test;

import mockit.Expectations;
import org.junit.Test;
import org.udg.caes.stockmarket.Portfolio;
import org.udg.caes.stockmarket.User;
import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;

import static org.junit.Assert.assertEquals;


/**
 * Created by Meriem on 14/01/2016.
 */
public class TestReplacePortfolio {
    Portfolio p1;
    Portfolio p2;
    User u1;
    User u2;

    @Test
    public void TestReplacePortfolioOK() throws ElementNotExists, ElementAlreadyExists {
        new Expectations(){{
            u1 = new User("u1");
            u2 = new User("u2");
            p1 = new Portfolio(u1,"p");
            p2 = new Portfolio(u2,"p");
            u1.addPortfolio(p1);
            u1.replacePortfolio(p2);
        }};
    }

    @Test
    public void TestReplacePortfolioNoExist() throws ElementNotExists, ElementAlreadyExists {
        new Expectations(){{
            u1 = new User("u1");
            u2 = new User("u2");
            p1 = new Portfolio(u1,"p");
            p2 = new Portfolio(u2,"p");
            u1.addPortfolio(p1);
            u1.replacePortfolio(p2);
        }};
        assertEquals(p2.getId().equals(u1.getPortfolio("p").getId()),true);
    }

}
