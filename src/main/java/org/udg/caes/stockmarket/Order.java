package org.udg.caes.stockmarket;

import org.udg.caes.stockmarket.NOTEST.OrderServiceImpl;

import java.util.Date;

/**
 * Created by imartin on 12/12/13.
 */
public class Order {
  private String id;

  Portfolio portfolio;

  int type;
  public static int MARKET = 1;
  public static int STOP_LOSS = 2;

  int quantity;
  double targetPrice;
  Date limit;
  String ticker;

  int status;
  public static int ACCEPTED = 1;
  public static int PROCESSING = 2;
  public static int COMPLETED = 3;
  public static int PARTIALLY_COMPLETED = 4;
  public static int CANCELLED = 5;
  public static int TIMEOUT = 6;
  public static int REJECTED = 7;

  private int efQuant;
  private double efPrice;
  private double lossPrice;

  Order(Portfolio pf, int q, double lp, Date li, String t) {
    status = Order.PROCESSING;
    portfolio = pf;
    quantity = q;
    type = STOP_LOSS;
    lossPrice = lp;
    ticker = t;
    limit = li;
  }

  public Order(Portfolio pf, int q, Date l, String t) {
    status = Order.PROCESSING;
    portfolio = pf;
    quantity = q;
    type = MARKET;
    ticker = t;
    limit = l;
  }

  /**
   * Send the order and a callback to be used once the stock service receives the response from the broker
   * @param ss
   * @return true if the order has been accepted
   * @throws NoSuchMethodException
   */
  public boolean send(OrderServiceImpl ss) throws NoSuchMethodException {
    return ss.send(this,
        BusinessService.class.getDeclaredMethod("processingCallback", Order.class),
        BusinessService.class.getDeclaredMethod("executionCallback", Order.class));
  }

  public String getId() {
    return id;
  }

  public void setId(String i) {
    this.id = i;
  }

  public String getTicker() {
    return ticker;
  }

  public int getEfQuant() {
    return efQuant;
  }

  public double getEfPrice() {
    return efPrice;
  }

  public Portfolio getPortfolio() {
    return portfolio;
  }

  public int getStatus() {
    return status;
  }

  public void setEfQuant(int eq) {
    this.efQuant = eq;
  }

  public void setEfPrice(double ep) {
    this.efPrice = ep;
  }

  public void setStatus(int s) {
    this.status = s;
  }

  public int getQuantity() {
    return quantity;
  }

  public int getType() {
    return type;
  }

  public double getLossPrice() {
    return lossPrice;
  }
}
