package org.udg.caes.stockmarket.NOTEST;

import java.util.ArrayList;

// This is a fake class

public class FakeEUR_SS_Impl extends OrderServiceImpl {

  FakeEUR_SS_Impl() {
    super("EUR");

    Fake_SM_Impl ibex35 = new Fake_SM_Impl("IBEX35");
    ibex35.addStocks(new ArrayList<String>() {{ add("TEL"); add("ITX"); }});
    Fake_SM_Impl frankfurt = new Fake_SM_Impl("Frankfurt");
    frankfurt.addStocks(new ArrayList<String>() {{ add("NSU"); add("SIE"); }});

    this.addMarket(ibex35);
    this.addMarket(frankfurt);
  }
}
