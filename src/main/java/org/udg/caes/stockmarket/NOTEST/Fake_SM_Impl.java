package org.udg.caes.stockmarket.NOTEST;

import org.udg.caes.stockmarket.Operation;
import org.udg.caes.stockmarket.StockMarket;
import org.udg.caes.stockmarket.exceptions.InvalidOperation;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.util.ArrayList;

// Fake class
public class Fake_SM_Impl implements StockMarket {

  String mName;
  ArrayList<String> mStocks = new ArrayList<String>();

  public Fake_SM_Impl(String name) {
    mName = name;
  }

  // This is a fake method that it is only here for the stub
  // In a real implementation stock names should be obtained from a remote service
  public void addStocks(ArrayList<String> stocks) {
    mStocks = stocks;
  }
public void addStocks(String stocks) {
    mStocks.add(stocks);
}

  public String getName() {
    return mName;
  }

  public Double getPrice(String name) throws StockNotFound {
    // This is fake code. Here we would need a connection to a broker
    if (mStocks.contains(name)) {
      return  50.0 + 500.0 * Math.random();
    }
    throw new StockNotFound();
  }

  public Operation buy(String name, int quantity) throws StockNotFound, InvalidOperation {
    // This is fake code. Here we would need a connection to a broker
    if (mStocks.contains(name)) {
      double r = Math.random();
      if (r > 0.95)
        throw new InvalidOperation();
      double price = this.getPrice(name);
      if (r > 0.5) {
        return new Operation(quantity, price, Operation.Status.COMPLETED);
      } else {
        return new Operation((int)(quantity * r), price, Operation.Status.PARTIAL);
      }
    }
    throw new StockNotFound();
  }

  public Operation sell(String name, int quantity) throws StockNotFound, InvalidOperation {
    // This is fake code. Here we would need a connection to a broker
    if (mStocks.contains(name)) {
      double r = Math.random();
      double price = this.getPrice(name);
      if (r > 0.95)
        throw new InvalidOperation();
      if (r > 0.5)
        return new Operation(quantity, price, Operation.Status.COMPLETED);
      return new Operation((int)(quantity * r), price, Operation.Status.PARTIAL);
    }
    throw new StockNotFound();
  }

  public boolean hasStock(String name) {
    return mStocks.contains(name);
  }

  public ArrayList<String> getAllStocks() {
    return mStocks;
  }

  public ArrayList<String> getAllStocks(String buscar) {
    ArrayList<String> ret = new ArrayList<String>();
    for (String s : mStocks)
      if (s.contains(buscar))
        ret.add(s);
    return ret;
  }
}
