package org.udg.caes.stockmarket.NOTEST;

import org.udg.caes.stockmarket.Order;
import org.udg.caes.stockmarket.OrderService;
import org.udg.caes.stockmarket.StockMarket;
import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 14/11/13
 * Time: 17:47
 * To change this template use File | Settings | File Templates.
 */
public class OrderServiceImpl implements OrderService {
  private Map<String, StockMarket> markets = new HashMap<String, StockMarket>();
  private HashMap<String, Order> pendingOrders = new HashMap<String, Order>();
  private HashMap<String, Method> pCallbacks = new HashMap<String, Method>();
  private String mName;
  private HashMap<String, Method> eCallbacks = new HashMap<String, Method>();

  public OrderServiceImpl(String name) {
    mName = name;
  }

  public String getName() {
    return mName;
  }

  public void addMarket(StockMarket sm) {
    // This is fake code, cannot be public
    // Markets should be obtained fro some remote service
    markets.put(sm.getName(), sm);
  }

  public Double getPrice(String name) throws StockNotFound {
    Double p = null;
    for (StockMarket sm: markets.values()) {
      try {
        p = sm.getPrice(name);
        if (p > 0.0) return p;
      } catch (StockNotFound e) {
      }
    }
    throw new StockNotFound();
  }

  public Collection<StockMarket> getMarkets() {
    return markets.values();
  }

  public boolean send(Order o, Method pc, Method ec) {
    pendingOrders.put(o.getId(), o);
    pCallbacks.put(o.getId(), pc);
    eCallbacks.put(o.getId(), ec);
    return true;
  }

  /**
   * This method should not exist in a real implementation. DO NOT TEST. BUT YOU CAN REFACTOR IT.
   * @param id
   * @throws InvocationTargetException
   * @throws IllegalAccessException
   */
  public void simulateOrderExecution(String id) throws InvocationTargetException, IllegalAccessException, StockNotFound {
    Order o = pendingOrders.get(id);
    Method m = eCallbacks.get(id);
    pendingOrders.remove(id);
    pCallbacks.remove(id);
    eCallbacks.remove(id);

    if (o.getType() == Order.MARKET)
      o.setEfPrice(this.getPrice(o.getTicker()));
    else
      o.setEfPrice(o.getLossPrice());

    if (Math.random() > 0.8) {
      o.setStatus(Order.COMPLETED);
      o.setEfQuant(o.getQuantity());
    }
    else {
      o.setStatus(Order.PARTIALLY_COMPLETED);
      o.setEfQuant((int)(Math.random() * o.getQuantity()));
    }
    m.invoke(null, o);
  }

  /**
   * This method should not exist in a real implementation. DO NOT TEST. BUT YOU CAN REFACTOR IT.
   * @param id
   * @throws InvocationTargetException
   * @throws IllegalAccessException
   */
  public void simulateOrderProcessing(String id) throws InvocationTargetException, IllegalAccessException {
    Order o = pendingOrders.get(id);
    Method m = pCallbacks.get(id);
    if (Math.random() > 0.1)
      o.setStatus(Order.ACCEPTED);
    else
      o.setStatus(Order.REJECTED);

    m.invoke(null, o);
  }

}
