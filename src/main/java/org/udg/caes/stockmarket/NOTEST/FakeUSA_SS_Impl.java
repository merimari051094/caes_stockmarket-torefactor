package org.udg.caes.stockmarket.NOTEST;

import java.util.ArrayList;

// This is a fake class
public class FakeUSA_SS_Impl extends OrderServiceImpl {

  FakeUSA_SS_Impl() {
    super("USA");

    Fake_SM_Impl nasdaq = new Fake_SM_Impl("NASDAQ");
    nasdaq.addStocks(new ArrayList<String>() {{ add("NVDA"); add("FB"); }});
    Fake_SM_Impl nyse = new Fake_SM_Impl("NYSE");
    nyse.addStocks(new ArrayList<String>() {{ add("AMD"); add("NOK"); }});

    this.addMarket(nasdaq);
    this.addMarket(nyse);
  }

}
