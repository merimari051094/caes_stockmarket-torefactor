package org.udg.caes.stockmarket;

import org.udg.caes.stockmarket.exceptions.ElementAlreadyExists;
import org.udg.caes.stockmarket.exceptions.ElementNotExists;
import org.udg.caes.stockmarket.exceptions.EntityNotFound;
import org.udg.caes.stockmarket.exceptions.InvalidOperation;
import org.udg.caes.stockmarket.exceptions.InvalidOperation;

import java.util.HashMap;

/**
 * This class has a fake implementation.
 * In a real example, some of the methods should access a database, so we don't want to unit test them
 * IMPORTANT: for the sake of simplicity, we consider that when an object is saved, all its
 * referenced objects are also saved, including collections containing objects.
 */
public interface PersistenceService {
  public User getUser(String id) throws EntityNotFound;
  public Portfolio getPortfolio(String id) throws EntityNotFound;
  public void saveUser(User u);
  public void savePortfolio(Portfolio p) throws EntityNotFound, ElementNotExists, ElementAlreadyExists, InvalidOperation;

  public void saveOrder(Order o);
  public Order getOrder(String id) throws EntityNotFound;
}
