package org.udg.caes.stockmarket;

/**
 * Created with IntelliJ IDEA.
 * User: imartin
 * Date: 15/11/13
 * Time: 10:36
 * To change this template use File | Settings | File Templates.
 */
public class Operation {
  int mQuantity;
  double mBuyPrice;
  Status mStatus;

  public enum Status {PENDING, COMPLETED, PARTIAL, DISCARDED}

  public Operation(int q, double p, Status s) {
    mQuantity = q;
    mBuyPrice = p;
    mStatus = s;
  }

  public Operation(int q, double p) {
    mQuantity = q;
    mBuyPrice = p;
    mStatus = Status.PENDING;
  }


  public int getQuantity() {
    return mQuantity;
  }

  public void setQuantity(int mQuantity) {
    this.mQuantity = mQuantity;
  }

  public double getBuyPrice() {
    return mBuyPrice;
  }

  public void setBuyPrice(double mBuyPrice) {
    this.mBuyPrice = mBuyPrice;
  }

  public Status getmStatus() {
    return mStatus;
  }

  public void setmStatus(Status mStatus) {
    this.mStatus = mStatus;
  }
}
