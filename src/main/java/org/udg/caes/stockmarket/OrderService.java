package org.udg.caes.stockmarket;

import org.udg.caes.stockmarket.exceptions.StockNotFound;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 */
public interface OrderService {
  public String getName();
  public Double getPrice(String name) throws StockNotFound;

  public Collection<StockMarket> getMarkets();

  public boolean send(Order o, Method m, Method processingCallback);
}
